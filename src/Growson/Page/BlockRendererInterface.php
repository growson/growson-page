<?php

namespace Growson\Page;

use Growson\Page\Model\LayoutBlock;

interface BlockRendererInterface
{
    public function render(LayoutBlock $block, array $extraParams = []);
}
