<?php

namespace Growson\Page\Model;

class PageLayout
{
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $template;

    /**
     * @var LayoutBlock[]|array
     */
    private $blocks;

    /**
     * @var Page[]|array
     */
    private $pages;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return PageLayout
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     *
     * @return PageLayout
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return Page[]|array
     */
    public function getPages()
    {
        return $this->pages;
    }

    /**
     * @param Page[]|array $pages
     *
     * @return PageLayout
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return LayoutBlock[]|array
     */
    public function getBlocks()
    {
        return $this->blocks;
    }

    /**
     * @param LayoutBlock[]|array $blocks
     *
     * @return PageLayout
     */
    public function setBlocks(array $blocks)
    {
        $this->blocks = $blocks;

        return $this;
    }
}
