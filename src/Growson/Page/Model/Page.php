<?php

namespace Growson\Page\Model;

class Page
{
    const TYPE_POST = 'post';

    private $id;

    /**
     * @var string
     */
    private $type;

    /**
     * @var PageLayout
     */
    private $layout;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Page
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return PageLayout
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param PageLayout $layout
     *
     * @return Page
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }
}
