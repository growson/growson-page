<?php

namespace Growson\Page\Model;

use Growson\Page\BlockRendererInterface;

class LayoutBlock
{
    private $id;

    /**
     * @var PageLayout
     */
    private $layout;

    /**
     * @var int
     */
    private $positionId;

    /**
     * @var string
     */
    private $name;

    /**
     * @var BlockRendererInterface
     */
    private $renderer;

    /**
     * @var string
     */
    private $template;

    /**
     * @var array
     */
    private $params;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return PageLayout
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param PageLayout $layout
     *
     * @return LayoutBlock
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;

        return $this;
    }

    /**
     * @return int
     */
    public function getPositionId()
    {
        return $this->positionId;
    }

    /**
     * @param int $positionId
     *
     * @return LayoutBlock
     */
    public function setPositionId($positionId)
    {
        $this->positionId = $positionId;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return LayoutBlock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return BlockRendererInterface
     */
    public function getRenderer()
    {
        return $this->renderer;
    }

    /**
     * @param BlockRendererInterface $renderer
     *
     * @return LayoutBlock
     */
    public function setRenderer($renderer)
    {
        $this->renderer = $renderer;

        return $this;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @param string $template
     *
     * @return LayoutBlock
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     *
     * @return LayoutBlock
     */
    public function setParams(array $params)
    {
        $this->params = $params;

        return $this;
    }
}
