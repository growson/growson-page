<?php

namespace Growson\Page;

use Growson\Page\Model\LayoutBlock;
use Growson\Page\Model\PageLayout;

interface TemplateRendererInterface
{
    public function render($template, array $params = null);

    public function renderLayout(PageLayout $layout, array $params = []);

    public function renderBlock(LayoutBlock $block, array $extraParams = []);
}
