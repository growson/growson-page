<?php

require_once __DIR__.'/autoload.php';

require_once __DIR__.'/classes/TemplateRenderer.php';
require_once __DIR__.'/classes/BlockRenderer.php';


$layout = new \Growson\Page\Model\PageLayout();
$layout->setTemplate(__DIR__.'/templates/layout.php');


$block1 = new \Growson\Page\Model\LayoutBlock();
$block1->setName('content-content');
$block1->setTemplate(__DIR__.'/templates/blocks/content.php');
$block1->setRenderer(new BlockRenderer());

$block2 = new \Growson\Page\Model\LayoutBlock();
$block2->setName('header-logo');
$block2->setTemplate(__DIR__.'/templates/blocks/logo.php');

$block3 = new \Growson\Page\Model\LayoutBlock();
$block3->setName('aside-menu');
$block3->setTemplate(__DIR__.'/templates/blocks/aside-menu.php');

$layout->setBlocks([$block1, $block2, $block3]);


$renderer = new TemplateRenderer();
$renderer->renderLayout($layout);
