<?php

if (file_exists(__DIR__ . '/../vendor/autoload.php')) {
    require __DIR__.'/../vendor/autoload.php';
} else {
    spl_autoload_register(function ($class) {
        $base_dir = __DIR__ . '/../src/';

        $file = $base_dir . str_replace('\\', '/', $class) . '.php';

        if (file_exists($file)) {
            require $file;
        }
    });
}
