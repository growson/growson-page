<?php

class BlockRenderer implements \Growson\Page\BlockRendererInterface
{
    public function render(\Growson\Page\Model\LayoutBlock $block, array $extraParams = [])
    {
        self::_render($block->getTemplate(), array_replace_recursive((array)$block->getParams(), $extraParams));
    }

    private static function _render($template, array $params)
    {
        extract($params, EXTR_SKIP);
        include $template;
    }
}
