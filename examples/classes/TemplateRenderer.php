<?php

class TemplateRenderer implements \Growson\Page\TemplateRendererInterface
{
    public function render($template, array $params = [])
    {
        self::_render($template, $params);
    }

    public function renderLayout(\Growson\Page\Model\PageLayout $layout, array $params = [])
    {
        self::_render($layout->getTemplate(), array_replace([
            'renderer' => $this,
            'blocks' => array_combine(array_map(function (\Growson\Page\Model\LayoutBlock $block) {
                return $block->getName();
            }, $layout->getBlocks()), $layout->getBlocks())
        ], $params));
    }

    public function renderBlock(\Growson\Page\Model\LayoutBlock $block, array $extraParams = [])
    {
        if ($block->getRenderer()) {
            $block->getRenderer()->render($block, $extraParams);
        } else {
            self::_render($block->getTemplate(), array_replace_recursive((array)$block->getParams(), $extraParams));
        }
    }

    private static function _render($template, array $params)
    {
        extract($params, EXTR_SKIP);
        include $template;
    }
}
