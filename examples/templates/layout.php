<?php
/**
 * @var TemplateRenderer $renderer
 * @var \Growson\Page\Model\LayoutBlock[]|array $blocks
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Title</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
</head>

<body>
<div class="container">
    <header class="section-header">
        <div class="block-header-logo">
            <?php $renderer->renderBlock($blocks['header-logo']) ?>
        </div>
    </header>
</div>

<div class="container">
    <div class="row">
        <aside class="section-aside col-md-3">
            <div class="block-aside-menu">
                <?php $renderer->renderBlock($blocks['aside-menu']) ?>
            </div>
        </aside>

        <div class="section-content col-md-9">
            <div class="block-content-content">
                <?php $renderer->renderBlock($blocks['content-content']) ?>
            </div>
        </div>
    </div>
</div>
</body>
</html>
